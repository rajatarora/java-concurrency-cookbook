package concurrent.chapter2.t04;

/**
 * @author Rajat Arora
 * 
 * Demonstrated Reentrant Lock mechanism. For complete description
 * see PrintJob.java
 *
 */
public class Job implements Runnable {

	private PrintQueue printQueue;
	
	public Job (PrintQueue printQueue) {
		this.printQueue = printQueue;
	}
	
	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName() + " is going to print a document.");
		printQueue.print(new Object());
		System.out.println(Thread.currentThread().getName() + " has printed the document.");
	}
}
