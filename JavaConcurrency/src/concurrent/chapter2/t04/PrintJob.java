package concurrent.chapter2.t04;

/**
 * @author Rajat Arora
 * 
 * Demonstrates Reentrant Lock mechanism.
 * 
 * Reentrant Lock is a mutual exclusion lock much like synchronized, but with
 * extra capabilities. 
 * 
 * In this example, 10 threads try to print documents by adding to the print
 * queue. Once a print job is started though, it acquires a lock, which remains
 * active until the job is completed. During this time, no other thread can
 * access the printer.
 *
 */
public class PrintJob {

	public static void main(String[] args) {
		
		PrintQueue printQueue = new PrintQueue();
		
		Thread[] threads = new Thread[10];
		
		for (int i = 0; i < 10; i++) {
			threads[i] = new Thread(new Job(printQueue));
		}
		
		for (int i = 0; i < 10; i++) {
			try {
				threads[i].start();
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
