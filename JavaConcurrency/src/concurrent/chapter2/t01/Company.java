package concurrent.chapter2.t01;

/**
 * @author Rajat Arora
 * 
 * This class is used to add money to an account. For complete description
 * of the example, see BasicSync.java
 *
 */
class Company implements Runnable {

	private Account account;
	
	public Company(Account account) {
		this.account = account;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			account.add();
		}
	}
}
