package concurrent.chapter2.t01;

/**
 * @author Rajat Arora
 * 
 * Demonstrates basic synchronization of methods.
 * 
 * When a method is made synchronized, only one thread can access it at 
 * one time. In this example, two methods add / subtract money at the same
 * time to the same account. 
 * 
 * We're adding and subtracting the _same_ amount of money from the account, 
 * so the ending balance of the account should be the same as the starting
 * balance. The synchronized version demonstrates the correct behavior, but
 * unsynchronized methods do not. 
 *
 */
class BasicSync {

	public static void main(String[] args) {
		
		Account account = new Account();
		account.setBalance();
		
		Bank bank = new Bank(account);
		Company company = new Company(account);
		
		Thread bankThread = new Thread(bank);
		Thread companyThread = new Thread(company);
		
		System.out.println("Starting Balance : " + account.getBalance());
		
		bankThread.start();
		companyThread.start();
		
		try {
			bankThread.join();
			companyThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Ending Balance : " + account.getBalance());
	}
}

/* 
 * With synchronized :
 * 
 * Starting Balance : 1000.0
 * Ending Balance : 1000.0
 * 
 * Without synchronized :
 * 
 * Starting Balance : 1000.0
 * Ending Balance : 29000.0 --> Can change!
 * 
 */
