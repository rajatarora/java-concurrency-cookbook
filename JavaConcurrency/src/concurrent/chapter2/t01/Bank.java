package concurrent.chapter2.t01;

/**
 * @author Rajat Arora
 * 
 * This class is used to subtract money from an account. For complete description
 * of the example, see BasicSync.java
 *
 */
class Bank implements Runnable {
	
	private Account account;
	
	public Bank(Account account) {
		this.account = account;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			account.subtract();
		}
	}
}
