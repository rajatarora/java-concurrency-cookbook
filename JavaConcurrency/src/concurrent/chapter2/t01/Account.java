package concurrent.chapter2.t01;

import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * This class represents a bank account. For complete description
 * of the example see BasicSync.java
 *
 */
class Account {

	private double balance;

	public double getBalance() {
		return balance;
	}

	public void setBalance() {
		this.balance = (double) 1000;
	}
	
	// run this with and without synchronized
	public synchronized void add() {
		double tmp = balance;
		try {
			TimeUnit.MILLISECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		tmp += (double) 1000;
		balance = tmp;
	}
	
	// run this with and without synchronized
	public synchronized void subtract() {
		double tmp = balance;
		try {
			TimeUnit.MILLISECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		tmp -= (double) 1000;
		balance = tmp;
	}
}
