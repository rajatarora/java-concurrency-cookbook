package concurrent.chapter2.t02;

/**
 * @author Rajat Arora
 * 
 * Represents the second ticket office. For complete description see
 * IndependentSync.java
 *
 */
public class TicketOffice2 implements Runnable {

	private Cinema cinema;
	
	public TicketOffice2(Cinema cinema) {
		this.cinema = cinema;
	}
	
	@Override
	public void run() {
		cinema.sellTicketsCinema2(2);
		cinema.sellTicketsCinema2(4);
		cinema.sellTicketsCinema1(2);
		cinema.sellTicketsCinema1(1);
		cinema.returnTicketsCinema2(2);
		cinema.sellTicketsCinema1(3);
		cinema.sellTicketsCinema2(2);
		cinema.sellTicketsCinema1(2);
	}
}
