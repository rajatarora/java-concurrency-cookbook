package concurrent.chapter2.t02;

/**
 * @author Rajat Arora
 * 
 * Represents the first ticket office. For complete description
 * see IndependentSync.java
 *
 */
public class TicketOffice1 implements Runnable {

	private Cinema cinema;
	
	public TicketOffice1(Cinema cinema) {
		this.cinema = cinema;
	}
	
	@Override
	public void run() {
		cinema.sellTicketsCinema1(3);
		cinema.sellTicketsCinema1(2);
		cinema.sellTicketsCinema2(2);
		cinema.returnTicketsCinema1(3);
		cinema.sellTicketsCinema1(5);
		cinema.sellTicketsCinema2(2);
		cinema.sellTicketsCinema2(2);
		cinema.sellTicketsCinema2(2);
	}
}
