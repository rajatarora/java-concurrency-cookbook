package concurrent.chapter2.t02;

/**
 * @author Rajat Arora
 * 
 * Represents a cinema with two screens. For complete description
 * see IndependentSync.java
 *
 */
class Cinema {

	private long vacanciesCinema1;
	private long vacanciesCinema2;
	
	// These are the two synchronization objects
	// Usually 'this' can be used if only one sync
	// object is required.
	private final Object controlCinema1 = new Object();
	private final Object controlCinema2 = new Object();
	
	public Cinema() {
		vacanciesCinema1 = 20L;
		vacanciesCinema2 = 20L;
	}
	
	public Boolean sellTicketsCinema1(long number) {
		synchronized(controlCinema1) {
			if (vacanciesCinema1 >= number) {
				vacanciesCinema1 -= number;
				return true;
			}
			return false;
		}
	}
	
	public Boolean sellTicketsCinema2(long number) {
		synchronized(controlCinema2) {
			if (vacanciesCinema2 >= number) {
				vacanciesCinema2 -= number;
				return true;
			}
			return false;
		}
	}
	
	public Boolean returnTicketsCinema1(long number) {
		synchronized(controlCinema1) {
			vacanciesCinema1 += number;
			return true;
		}
	}
	
	public Boolean returnTicketsCinema2(long number) {
		synchronized(controlCinema2) {
			vacanciesCinema1 += number;
			return true;
		}
	}
	
	public long getVacanciesCinema1() {
		return vacanciesCinema1;
	}
	
	public long getVacanciesCinema2() {
		return vacanciesCinema2;
	}
}
