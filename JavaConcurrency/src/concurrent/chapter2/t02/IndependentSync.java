package concurrent.chapter2.t02;

/**
 * @author Rajat Arora
 * 
 * Demonstrates usage of multiple synchronized objects. 
 * 
 * In this example, we protect two blocks of code with synchronized keyword
 * using two different objects - controlCinema1 and controlCinema2. Two different
 * threads can access the two synchronized blocks independently.
 * 
 * Here, there are two cinemas and two ticket offices. Any ticket office can sell
 * tickets for any cinema. However, to keep track of vacant seats, both cinemas
 * are synchronized with two different objects.
 * 
 * We could have done with a single synchronization object, but that would have 
 * meant that while office1 was booking tickets for cinema1, office2 could not have
 * booked tickets for cinema2.
 *
 */
public class IndependentSync {

	public static void main (String[] args) {
		
		Cinema cinema = new Cinema();
		
		TicketOffice1 office1 = new TicketOffice1(cinema);
		TicketOffice2 office2 = new TicketOffice2(cinema);
		
		Thread officeThread1 = new Thread(office1);
		Thread officeThread2 = new Thread(office2);
		
		officeThread1.start();
		officeThread2.start();
		
		try {
			officeThread1.join();
			officeThread2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Cinema 1 Vacancies : " + cinema.getVacanciesCinema1());
		System.out.println("Cinema 2 Vacancies : " + cinema.getVacanciesCinema2());
	}
}
