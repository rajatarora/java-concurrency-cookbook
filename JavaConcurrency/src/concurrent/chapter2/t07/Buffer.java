package concurrent.chapter2.t07;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the classic Producer-Consumer problem implemented
 * using {@link java.util.concurrent.locks.Condition}.
 * 
 * This class represents the object buffer. There are two 'conditions'
 * that need to be checked - one that there is space in buffer, other
 * that there are more objects waiting to be put into buffer. 
 * 
 * Basically multiple 'conditions' are applied to the locks, which 
 * wait and signal to each other accordingly.
 *
 */
class Buffer {

	private LinkedList<String> buffer;
	private int maxSize;
	
	private ReentrantLock lock;
	
	/**
	 * This checks if there are more lines available.
	 */
	private Condition lines;
	
	
	/**
	 * This checks if there is space available in the buffer.
	 */
	private Condition space;
	
	private boolean pendingLines;
	
	Buffer(int maxSize) {
		this.buffer = new LinkedList<>();
		this.maxSize = maxSize;
		this.lock = new ReentrantLock();
		this.lines = lock.newCondition();
		this.space = lock.newCondition();
		this.pendingLines = true;
	}
	
	boolean hasPendingLines() {
		return pendingLines || buffer.size() > 0;
	}
	
	void setPendingLines(boolean pendingLines) {
		this.pendingLines = pendingLines;
	}
	
	void insert(String line) {
		lock.lock();
		
		// If buffer is full, wait for a signal on the space condition.
		try {
			if (buffer.size() == maxSize) {
				space.await();
			}
			
			// Once signal is received, add a line to the buffer
			buffer.offer(line);
			System.out.println(Thread.currentThread().getName() + " inserted line. "
					+ "Buffer size : " + buffer.size());
			
			// Since a line is available, signal all threads waiting on the line
			// condition that it is available for taking.
			lines.signalAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
	}
	
	String get() {
		String line = null;
		lock.lock();
		try {
			// While the buffer is empty, wait for a signal on the line condition
			while ((buffer.size() == 0) && hasPendingLines()) {
				lines.await();
			}
			
			// Once signal is received and lines are pending on the buffer,
			// take one.
			if (hasPendingLines()) {
				line = buffer.poll();
				System.out.println(Thread.currentThread().getName() + " read line. "
						+ "Buffer size : " + buffer.size());
				
				// Since one line is taken, there is space available. Signal all threads
				// waiting on the space condition to wake up and add some lines!
				space.signalAll();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			lock.unlock();
		}
		return line;
	}
}
