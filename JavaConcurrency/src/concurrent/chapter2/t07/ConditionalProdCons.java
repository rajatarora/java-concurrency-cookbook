package concurrent.chapter2.t07;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the classic Producer-Consumer problem implemented
 * using {@link java.util.concurrent.Condition}. 
 * 
 * Here one producer and three consumers act in tandem to read from / 
 * write to the buffer.
 *
 */
class ConditionalProdCons {

	public static void main(String[] args) {
		
		FileMock mock = new FileMock(100, 10);
		Buffer buffer = new Buffer(20);
		
		int numConsumers = 3;
		
		Producer producer = new Producer(mock, buffer);
		Consumer[] consumers = new Consumer[numConsumers];
		
		Thread producerThread = new Thread(producer, "Producer");
		Thread[] consumerThreads = new Thread[numConsumers];
		
		for (int i = 0; i < numConsumers; i++) {
			consumers[i] = new Consumer(buffer);
			consumerThreads[i] = new Thread(consumers[i], "Consumer_" + i);
		}
		
		producerThread.start();
		for (int i = 0; i < numConsumers; i++) {
			consumerThreads[i].start();
		}
	}
}
