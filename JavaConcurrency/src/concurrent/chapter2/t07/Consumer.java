package concurrent.chapter2.t07;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the classic Producer-Consumer problem implemented
 * using {@link java.util.concurrent.Condition}.
 * 
 * This class represents the Consumer. It keeps reading lines from
 * the buffer as long as they are available.
 *
 */
class Consumer implements Runnable {

	private Buffer buffer;
	
	public Consumer (Buffer buffer) {
		this.buffer = buffer;
	}
	
	private void processLine (String line) {
		Random random = new Random(Thread.currentThread().getId());
		try {
			TimeUnit.MILLISECONDS.sleep(random.nextInt(10));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		while (buffer.hasPendingLines()) {
			String line = buffer.get();
			processLine(line);
		}
	}
}
