package concurrent.chapter2.t07;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the classic Producer-Consumer problem implemented
 * using {@link java.util.concurrent.Condition}. 
 * 
 * This class represents the Producer. It reads the mock file and
 * fills data into the buffer.
 *
 */
public class Producer implements Runnable {

	private FileMock mock;
	private Buffer buffer;
	
	public Producer (FileMock mock, Buffer buffer) {
		this.mock = mock;
		this.buffer = buffer;
	}
	
	@Override
	public void run() {
		buffer.setPendingLines(true);
		while (mock.hasMoreLines()) {
			String line = mock.getLine();
			buffer.insert(line);
		}
		buffer.setPendingLines(false);
	}
}
