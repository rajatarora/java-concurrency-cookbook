package concurrent.chapter2.t03;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the classic Producer-Consumer problem. For complete
 * description see ProdCons.java
 * 
 * This class represents the Producer. It sets one data-point in storage,
 * waits for a random time, and then does it again. 
 *
 */
public class Producer implements Runnable {

	private EventStorage storage;
	
	public Producer (EventStorage storage) {
		this.storage = storage;
	}
	
	@Override
	public void run() {
		Random random = new Random(Thread.currentThread().getId());
		for (int i = 0; i < 100; i++) {
			storage.set();
			try {
				// Try setting different values here with respect to consumer.
				TimeUnit.MILLISECONDS.sleep(random.nextInt(45));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
