package concurrent.chapter2.t03;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the classic Producer-Consumer problem. For complete
 * description see ProdCons.java
 * 
 * This class represents the Consumer. It gets one data-point from storage,
 * waits for a random time, and does it again.
 *
 */
class Consumer implements Runnable {

	private EventStorage storage;
	
	public Consumer (EventStorage storage) {
		this.storage = storage;
	}
	
	@Override
	public void run() {
		Random random = new Random(Thread.currentThread().getId());
		for (int i = 0; i < 100; i++) {
			storage.get();
			try {
				// Try setting different values here with respect to Producer.
				TimeUnit.MILLISECONDS.sleep(random.nextInt(60));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
