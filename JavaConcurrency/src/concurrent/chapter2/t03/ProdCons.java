package concurrent.chapter2.t03;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the classic Producer-Consumer problem. 
 * 
 * A producer puts data into a storage buffer, while a consumer removes
 * from it. Producer cannot put data when buffer is full, and consumer
 * cannot remove when buffer is empty. 
 * 
 * We define a buffer called EventStorage, and decide upon its maximum size.
 * The producer "waits" until the buffer has some space, puts more data, 
 * and "notifies" all other threads. Similarly, the consumer "waits" until
 * the buffer gets some data, removes it and "notifies" all other threads.
 * 
 * You will notice that the number of objects in the buffer are never 
 * more than the max size, nor do they ever go below zero.
 *
 */
public class ProdCons {

	public static void main(String[] args) {
		
		EventStorage storage = new EventStorage();
		Producer producer = new Producer(storage);
		Consumer consumer = new Consumer(storage);
		
		Thread producerThread = new Thread(producer);
		Thread consumerThread = new Thread(consumer);
		
		producerThread.start();
		consumerThread.start();
		
		try {
			producerThread.join();
			consumerThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
