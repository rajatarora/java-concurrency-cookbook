package concurrent.chapter2.t03;

import java.util.Date;
import java.util.PriorityQueue;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the classic Producer-Consumer problem. For complete
 * description see ProdCons.java
 * 
 * This class represents the storage buffer. It has a priority queue
 * as well as a maxSize definition. Two synchronized functions set()
 * and get() are used by producers and consumers respectively.
 *
 */
public class EventStorage {

	private int maxSize;
	private PriorityQueue<Date> storage;
	
	public EventStorage() {
		maxSize = 10;
		storage = new PriorityQueue<>();
	}
	
	public synchronized void set() {
		while (storage.size() == maxSize) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		storage.offer(new Date());
		System.out.println("Set : " + storage.size());
		notifyAll();
	}
	
	public synchronized void get() {
		while (storage.size() == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Date date = storage.poll();
		System.out.println("Retrieved : " + date + ", Size : " + storage.size());
		notifyAll();
	}
}
