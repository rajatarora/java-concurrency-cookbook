package concurrent.chapter2.t05;

/**
 * @author Rajat Arora
 * 
 * Demonstrates a ReadWriteLock. Here, 5 reader threads try
 * to read values of a variable, which can only be modified
 * by a writer thread. The reader threads obtain a ReadLock
 * on the variable while the writer thread obtains a WriteLock.
 * 
 * Multiple threads can read the variable value, but only
 * one thread can write to it at a time.
 *
 */
public class RWLock {

	public static void main (String[] args) {
		
		PricesInfo pricesInfo = new PricesInfo(1.0, 2.0);
		
		Thread[] readerThreads = new Thread[5];
		Thread writerThread;
		
		for (int i = 0; i < 5; i++) {
			readerThreads[i] = new Thread(new Reader(pricesInfo));
		}
		writerThread = new Thread(new Writer(pricesInfo));
		
		for (int i = 0; i < 5; i++) {
			readerThreads[i].start();
		}
		writerThread.start();
	}
}
