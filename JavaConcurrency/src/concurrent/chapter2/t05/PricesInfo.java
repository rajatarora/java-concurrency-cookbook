package concurrent.chapter2.t05;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Rajat Arora
 * 
 * Demonstrates a ReadWriteLock. For complete description
 * see RWLock.java
 *
 */
public class PricesInfo {

	private double price1;
	private double price2;
	
	private final ReadWriteLock lock = new ReentrantReadWriteLock();
	
	public PricesInfo(double price1, double price2) {
		this.price1 = price1;
		this.price2 = price2;
	}
	
	public double getPrice1() {
		lock.readLock().lock();
		double value = price1;
		lock.readLock().unlock();
		return value;
	}
	
	public double getPrice2() {
		lock.readLock().lock();
		double value = price2;
		lock.readLock().unlock();
		return value;
	}
	
	public void setPrices(double price1, double price2) {
		lock.writeLock().lock();
		this.price1 = price1;
		this.price2 = price2;
		lock.writeLock().unlock();
	}
}
