package concurrent.chapter2.t05;

import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates a ReadWriteLock. For complete description
 * see RWLock.java
 *
 */
public class Writer implements Runnable {

	private PricesInfo pricesInfo;
	
	public Writer (PricesInfo pricesInfo) {
		this.pricesInfo = pricesInfo;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 3; i++) {
			System.out.println("Attempting to modify prices.");
			pricesInfo.setPrices(Math.random()*20, Math.random()*17);
			System.out.println("Prices Modified.");
			try {
				TimeUnit.MILLISECONDS.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
