package concurrent.chapter2.t05;

import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates a ReadWriteLock. For complete description
 * see RWLock.java
 *
 */
public class Reader implements Runnable {

	private PricesInfo pricesInfo;
	
	public Reader (PricesInfo pricesInfo) {
		this.pricesInfo = pricesInfo;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println(Thread.currentThread().getName() + " Price 1 : " + pricesInfo.getPrice1());
			System.out.println(Thread.currentThread().getName() + " Price 2 : " + pricesInfo.getPrice2());
			try {
				TimeUnit.MILLISECONDS.sleep(3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
