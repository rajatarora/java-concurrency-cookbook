package concurrent.chapter2.t06;

import java.util.concurrent.TimeUnit;

import concurrent.chapter2.t06.Job;
import concurrent.chapter2.t06.PrintQueue;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the fair Reentrant Lock mechanism. This is a modified version of 
 * the example presented in {@link concurrent.chapter2.t04}
 * 
 * Here the ReentrantLock is made out to be fair. This means that, once a lock is
 * released and multiple threads are waiting for it, the lock is passed on to the
 * thread which has been waiting for the maximum time.
 * 
 * In case of unfair lock (default), the OS decides which thread to wake up. JVM
 * does not guarantee the order of execution in this case.
 *
 */
public class PrintJob {

	public static void main(String[] args) {
		
		PrintQueue printQueue = new PrintQueue();
		
		Thread[] threads = new Thread[3];
		
		for (int i = 0; i < 3; i++) {
			threads[i] = new Thread(new Job(printQueue));
		}
		
		for (int i = 0; i < 3; i++) {
			try {
				threads[i].start();
				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

/*
 * OUTPUT : UNFAIR
 * Notice that the thread which released the lock
 * immediately re-acquires it.
 * 
 * Thread-0 is going to print a document.
 * Thread-0 printing for 1 seconds
 * Thread-1 is going to print a document.
 * Thread-2 is going to print a document.
 * Thread-0 printing for 4 seconds
 * Thread-0 has printed the document.
 * Thread-1 printing for 2 seconds
 * Thread-1 printing for 0 seconds
 * Thread-1 has printed the document.
 * Thread-2 printing for 4 seconds
 * Thread-2 printing for 3 seconds
 * Thread-2 has printed the document.
 * 
 * OUTPUT : FAIR
 * Notice that once a lock is released, it is
 * passed on to the thread which has been waiting
 * for the most time.
 * 
 * Thread-0 is going to print a document.
 * Thread-0 printing for 3 seconds
 * Thread-1 is going to print a document.
 * Thread-2 is going to print a document.
 * Thread-1 printing for 0 seconds
 * Thread-2 printing for 1 seconds
 * Thread-0 printing for 3 seconds
 * Thread-0 has printed the document.
 * Thread-1 printing for 1 seconds
 * Thread-1 has printed the document.
 * Thread-2 printing for 3 seconds
 * Thread-2 has printed the document.
 * 
 */