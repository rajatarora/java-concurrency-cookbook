package concurrent.chapter2.t06;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the fair Reentrant Lock mechanism. For complete description
 * see PrintJob.java
 *
 */
public class PrintQueue {

	// true = fair lock
	private final Lock queueLock = new ReentrantLock(true);
	
	public void print(Object toPrint) {
		
		queueLock.lock();
		
		/*
		 * It is recommended that the lock() method call is immediately
		 * followed by a try block, and the unblock() method call is 
		 * called inside the finally block, so that we can be sure that
		 * once a lock is acquired, it will always be released.
		 * 
		 */
		
		try {
			long duration = (long)(Math.random()*5000);
			System.out.println(Thread.currentThread().getName() + " printing for " + duration/1000 + " seconds");
			TimeUnit.MILLISECONDS.sleep(duration);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			queueLock.unlock();
		}
		
		queueLock.lock();
		
		try {
			long duration = (long)(Math.random()*5000);
			System.out.println(Thread.currentThread().getName() + " printing for " + duration/1000 + " seconds");
			TimeUnit.MILLISECONDS.sleep(duration);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			queueLock.unlock();
		}
	}
}
