package concurrent.chapter1.t01;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

class Calculator implements Runnable {

	private int number;
	
	private Calculator(int number) {
		this.number = number;
	}
	
	@Override
	public void run() {
		for (int i = 1; i <= 100; i++) {
			System.out.printf("%s: %d * %d = %d \n", Thread.currentThread().getName(), 
					number, i, number*i);
		}
	}
	
	// this is the main execution thread
	public static void main(String[] args) {
		second();
	}
	
	/**
	 * Starts 10 threads
	 */
	static void first() {
		for (int i = 1; i <= 10; i++) {
			Calculator calculator = new Calculator(i);
			Thread thread = new Thread(calculator); // no execution thread is created at this point
			
			// here another execution thread is created
			// as many execution threads as there are calls to start()
			thread.start();
		}
	}
	
	
	/**
	 * Establishes name and priority for 10 threads and displays their status until they finish.
	 * The results are written in a log file. Whenever a thread changes its state, it is logged.
	 * Half the threads have max priority, half have min priority. It is seen that higher priority threads
	 * block lower priority threads, and generally finish first.
	 */
	private static void second() {
		
		// The 10 threads
		Thread threads[] = new Thread[10];
		
		// The 10 thread statuses
		Thread.State states[] = new Thread.State[10];
		
		// Create threads
		for (int i = 0; i < 10; i++) {
			threads[i] = new Thread(new Calculator(i));
			if (i %2 == 0) {
				threads[i].setPriority(Thread.MAX_PRIORITY);
			}
			else {
				threads[i].setPriority(Thread.MIN_PRIORITY);
			}
			threads[i].setName("Thread: " + i);
		}
		
		// Log the evolution of these threads
		try {
			FileWriter file = new FileWriter(System.getProperty("user.dir") + File.separator + UUID.randomUUID().toString() + ".txt");
			PrintWriter pw = new PrintWriter(file);
			
			// Store initial states. Everything is NEW
			for (int i = 0; i < 10; i++) {
				pw.println("State of " + threads[i].getName() + ": " + threads[i].getState());
				states[i] = threads[i].getState();
			}
			
			// Start all threads
			for (int i = 0; i < 10; i++) {
				threads[i].start();
			}
			
			// Loop until all threads are finished
			Boolean finish = false;
			while (!finish) {
				for (int i = 0; i < 10; i++) {
					if (threads[i].getState() != states[i]) {
						// Write to log as soon as a thread changes state
						writeThreadInfo(pw, threads[i], states[i]);
						states[i] = threads[i].getState();
					}
				}
				
				finish = true;
				for (int i = 0; i < 10; i++) {
					finish = finish && (threads[i].getState() == Thread.State.TERMINATED);
				}
			}
			
			pw.close();
			file.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void writeThreadInfo(PrintWriter pw, Thread thread, Thread.State oldState) {
		pw.println("ID: " + thread.getName());
		pw.println("Priority: " + thread.getPriority());
		pw.println("Old State: " + oldState);
		pw.println("New State: " + thread.getState());
		pw.println("************************************");
	}
}
