package concurrent.chapter1.t11;

import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the usage of ThreadFactory to create new threads.
 * 
 * Simply create a new class implementing ThreadFactory interface, and override
 * the newThread method. This way you can add default behavior to every thread
 * you want to create in the application.
 *
 */
public class FactoryExample {

	public static void main(String[] args) {
		
		MyThreadFactory factory = new MyThreadFactory("Factory");
		Task task = new Task();
		
		Thread thread;
		for (int i = 0; i < 10; i++) {
			thread = factory.newThread(task);
			thread.start();
			try {
				TimeUnit.MILLISECONDS.sleep(500);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println(factory.getStats());
	}
}
