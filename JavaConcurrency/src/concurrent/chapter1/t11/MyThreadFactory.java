package concurrent.chapter1.t11;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadFactory;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the usage of ThreadFactory to create new threads. For complete
 * description see FactoryExample.java
 *
 */
public class MyThreadFactory implements ThreadFactory {
	
	private int counter;
	private String name;
	private List<String> stats;
	
	public MyThreadFactory(String name) {
		this.counter = 0;
		this.stats = new ArrayList<>();
		this.name = name;
	}

	// This is where we personalize thread creation
	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r, name + "-Thread_" + counter);
		counter++;
		stats.add("Created thread: "
				+ t.getName()
				+ " on "
				+ new Date());
		return t;
	}
	
	public String getStats() {
		StringBuilder result = new StringBuilder();
		for (String s : stats) {
			result.append(s).append(System.lineSeparator());
		}
		return result.toString();
	}
}
