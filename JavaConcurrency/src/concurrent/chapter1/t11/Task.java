package concurrent.chapter1.t11;

import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the usage of ThreadFactory to create new threads. For complete
 * description see FactoryExample.java
 *
 */
public class Task implements Runnable {

	@Override
	public void run() {
		System.out.println("Task started.");
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
