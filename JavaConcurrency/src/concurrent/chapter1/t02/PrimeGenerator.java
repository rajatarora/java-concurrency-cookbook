package concurrent.chapter1.t02;

/**
 * @author Rajat Arora
 * Demonstrates the usage of thread interruption. 
 * Starts one thread, lets it run for some time
 * and then calls thread.interrupt()
 * 
 * The behavior of interruption is coded into the run() method.
 * A thread can choose to ignore calls to thread.interrupt(),
 * but this is not expected behavior
 *
 */
public class PrimeGenerator extends Thread {

	@Override
	public void run() {
		
		Long number = 1L;
		
		while (true) {
			if (isPrime(number)) {
				System.out.println(number + " is Prime.");
			}
			
			// Handling the interruption
			if (isInterrupted()) {
				System.out.println("Thread has been interrupted.");
				return;
			}
			
			number++;
		}
	}
	
	private Boolean isPrime(Long number) {
		
		if (number <= 2) {
			return true;
		}
		
		Long sqrt = (long) Math.sqrt(number);
		
		for (int i = 2; i < sqrt; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		
		Thread generator = new PrimeGenerator();
		generator.start();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		generator.interrupt();
		
	}
}
