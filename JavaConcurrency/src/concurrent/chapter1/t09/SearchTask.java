package concurrent.chapter1.t09;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates ThreadGroups. For full description see Group.java
 *
 */
public class SearchTask implements Runnable {

	private Result result;
	
	public SearchTask(Result result) {
		this.result = result;
	}
	
	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		System.out.println(name + " started.");
		try {
			doTask();
			result.setName(name);
		} catch (InterruptedException e) {
			System.out.println(name + " interrupted.");
			return;
		}
		System.out.println(name + " ended.");
	}
	
	private void doTask() throws InterruptedException {
		Random random = new Random(new Date().getTime());
		int value = (int) (random.nextDouble()*100+20);
		System.out.println("Thread : " + Thread.currentThread().getName() + " : " + value);
		TimeUnit.SECONDS.sleep(value);
	}
}
