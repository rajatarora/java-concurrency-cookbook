package concurrent.chapter1.t09;

/**
 * @author Rajat Arora
 *
 * Demonstrates ThreadGroups. For full description see Group.java
 * 
 */
public class Result {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
