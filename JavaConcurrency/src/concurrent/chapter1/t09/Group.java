package concurrent.chapter1.t09;

import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the usage of ThreadGroups. Here a bunch of threads are added to the
 * same thread group, and then all of them are interrupted at once using the 
 * threadGroup.interrupt() method.
 * 
 * One thread group can be composed of other thread groups : 
 * new ThreadGroup (name) vs new ThreadGroup(TG parent, name)
 * This can result in a tree like structure.
 * 
 * activeCount() : no. of active threads in the group
 * activeGroupCount() : no. of active thread groups in the group
 * parent() : returns the parent of this group
 *
 */
public class Group {

	public static void main (String[] args) {
		
		// Create a new group
		ThreadGroup tgroup = new ThreadGroup("Searcher");
		
		Result result = new Result();
		SearchTask searchTask = new SearchTask(result);
		
		// Activate 10 threads
		for (int i = 0; i < 10; i++) {
			Thread thread = new Thread(tgroup, searchTask);
			thread.start();
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("No. of threads : " + tgroup.activeCount());
		tgroup.list();
		
		Thread[] threads = new Thread[tgroup.activeCount()];
		tgroup.enumerate(threads);
		
		for (int i = 0; i < tgroup.activeCount(); i++) {
			System.out.println(threads[i].getName() + " : " + threads[i].getState());
		}
		
		// Wait for at least one thread to finish
		waitFinish(tgroup);
		
		// Once at least one thread finishes, interrupt all threads in the group.
		tgroup.interrupt();
	}
	
	private static void waitFinish(ThreadGroup tgroup) {
		
		while (tgroup.activeCount() > 9) {
			System.out.println("Active Threads : " + tgroup.activeCount());
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
