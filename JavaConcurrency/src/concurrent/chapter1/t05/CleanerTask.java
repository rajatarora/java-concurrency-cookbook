package concurrent.chapter1.t05;

import java.util.Date;
import java.util.Deque;

/**
 * @author Rajat Arora
 * 
 * Demonstrates daemon threads
 *
 */
@SuppressWarnings("InfiniteLoopStatement")
public class CleanerTask extends Thread {

	private Deque<Event> deque;
	
	public CleanerTask(Deque<Event> deque) {
		this.deque = deque;
		
		// Daemon threads are low priority
		// They do not prevent the JVM from exiting while they're still running
		// User threads - while they are running, will not let JVM exit
		// Example - GC
		// When no other user threads are running, JVM terminates the daemon threads and exits
		setDaemon(true);
	}
	
	@Override
	public void run() {

        //noinspection InfiniteLoopStatement
        while (true) {
			Date date = new Date();
			clean(date);
		}
		
	}
	
	
	/**
	 * @param date
	 * 
	 * Cleans all tasks which were started more than 10 seconds ago
	 * 
	 */
	private void clean(Date date) {
		long difference;
		boolean delete;
		
		if (deque.size() == 0) {
			return;
		}
		
		delete = false;
		
		do {
			Event e = deque.getLast();
			difference = date.getTime() - e.getDate().getTime();
			
			if (difference > 10000) {
				System.out.println("Cleaned: " + e.getEvent());
				deque.removeLast();
				delete = true;
			}
		} while (difference > 10000);
		
		if (delete) {
			System.out.println("Deque size: " + deque.size());
		}
	}
}
