package concurrent.chapter1.t05;

import java.util.Date;
import java.util.Deque;

/**
 * @author Rajat Arora
 * 
 * Demonstrates daemon threads
 *
 */
public class WriterTask implements Runnable {

	private Deque<Event> deque;
	
	public WriterTask(Deque<Event> deque) {
		this.deque = deque;
	}
	
	@Override
	public void run() {
		for (int i = 1; i <= 100; i++) {
			Event event = new Event();
			event.setDate(new Date());
			event.setEvent("Event #" + i);
			deque.addFirst(event);
			System.out.println(Thread.currentThread().getName() + " Generated: " + event.getEvent());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
