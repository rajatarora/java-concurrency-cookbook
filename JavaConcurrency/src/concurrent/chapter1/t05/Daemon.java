package concurrent.chapter1.t05;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author Rajat Arora
 * 
 * Demonstrates daemon threads.
 *
 */
class Daemon {

	public static void main (String[] args) {
		
		Deque<Event> events = new ArrayDeque<>();
		
		for (int i = 1; i <=3; i++) {
			Thread thread = new Thread(new WriterTask(events));
			thread.start();
		}
		
		CleanerTask cleaner = new CleanerTask(events);
		cleaner.start();
	}
}
