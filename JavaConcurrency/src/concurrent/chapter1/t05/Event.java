package concurrent.chapter1.t05;

import java.util.Date;

/**
 * @author Rajat Arora
 * 
 * Demonstrates daemon threads
 *
 */
class Event {
	
	private Date date;
	private String event;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
}
