package concurrent.chapter1.t10;

import java.util.Random;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the mechanism of catching uncaught exceptions inside
 * a thread in a ThreadGroup. For complete description see Uncontrolled.java
 *
 */
public class Task implements Runnable {

	@Override
	public void run() {
		int result;
		Random random = new Random(Thread.currentThread().getId());
		while(true) {
			
			// Generating random numbers and dividing by it. One of them
			// is bound to be zero.
			result = 1000 / (int)((random.nextDouble()*1000));
			System.out.println(result);
			
			// In case of divide-by-zero, the group's uncaught exception handler
			// will kick in, and will interrupt all threads.
			if(Thread.currentThread().isInterrupted()) {
				System.out.println("Thread interrupted: " + Thread.currentThread().getId());
				return;
			}
		}
	}
}
