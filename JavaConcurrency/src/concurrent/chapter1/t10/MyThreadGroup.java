package concurrent.chapter1.t10;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the mechanism of catching uncaught exceptions inside
 * a thread in a ThreadGroup. For complete description see Uncontrolled.java
 *
 */
public class MyThreadGroup extends ThreadGroup {

	public MyThreadGroup(String name) {
		super(name);
	}
	
	// That's where magic happens! Overridden uncaughtException method
	// This will apply to all thread in the group.
	@Override
	public void uncaughtException(Thread t, Throwable th) {
		System.out.println(t.getName() + " has thrown an exception.");
		System.out.println("Interrupting the rest of the threads.");
		interrupt();
	}
}
