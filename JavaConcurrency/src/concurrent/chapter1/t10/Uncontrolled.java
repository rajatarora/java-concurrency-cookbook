package concurrent.chapter1.t10;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the mechanism of catching uncaught exceptions inside
 * a thread in a ThreadGroup.
 * 
 * The example simulates a divide-by-zero inside one of the three threads
 * in the group. The exception handler resides not inside the thread, but 
 * in the thread group. 
 * 
 * There is a hierarchy of how an uncaught exception is handled in a thread.
 * First, the JVM tries to find a handler attached to a thread, if not found, 
 * it moves to the thread group. If still not found, the JVM looks for a default
 * handler. Otherwise, the JVM writes the stack trace to the console and exits
 * the program.
 *
 */
public class Uncontrolled {

	public static void main(String[] args) {
		
		MyThreadGroup mtGroup = new MyThreadGroup("MyGroup");
		Task task = new Task();
		
		for (int i =0; i <= 2; i++) {
			Thread t = new Thread(mtGroup, task);
			t.start();
		}
	}
}
