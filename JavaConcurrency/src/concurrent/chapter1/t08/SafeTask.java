package concurrent.chapter1.t08;

import java.util.Date;

/**
 * @author Rajat Arora
 * 
 * Demostrates the use of ThreadLocal variables. For description see
 * UnsafeTask class.
 *
 */
public class SafeTask implements Runnable {

	private static ThreadLocal<Date> date = ThreadLocal.withInitial(() -> new Date());
	
	@Override
	public void run() {
		
		System.out.println("Start : " + Thread.currentThread().getName() + " : " + date.get());
		
		try {
			Thread.sleep((int)Math.rint(Math.random()*10000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Finish : " + Thread.currentThread().getName() + " : " + date.get());
	}
	
	public static void main(String[] args) {
		
		SafeTask safeTask = new SafeTask();
		for (int i = 0; i < 3; i++) {
			Thread t = new Thread(safeTask);
			t.start();
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
