package concurrent.chapter1.t08;

import java.util.Date;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the usage of ThreadLocal variables
 * 
 * As any object implementing Runnable can be used by multiple threads at once,
 * it's state can be accessed by multiple threads. Example ::
 * 
 * t1 = new Thread(runnable1) and t2 = new Thread(runnable1) - both can access the 
 * state of runnable1 and can modify it. 
 * 
 * Using ThreadLocal variables means that the state of those variables can only 
 * be modified by that thread which created it.
 *
 */
public class UnsafeTask implements Runnable {

    @Override
	public void run() {

        Date date = new Date();
		
		System.out.println("Start : " + Thread.currentThread().getName() + " : " + date);
		
		try {
			Thread.sleep((int)Math.rint(Math.random()*10000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Finish : " + Thread.currentThread().getName() + " : " + date);
	}
	
	public static void main(String[] args) {
		
		UnsafeTask unsafeTask = new UnsafeTask();
		
		for (int i = 0; i < 3; i++) {
			Thread t = new Thread(unsafeTask);
			t.start();
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
