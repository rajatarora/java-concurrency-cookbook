package concurrent.chapter1.t03;

import java.io.File;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the usage of InterruptedException. This should be thrown
 * in case the logic implemented by a thread is complex and interruption
 * can result in unintended consequences.
 * 
 * The class searches for all instances of a particular file
 * recursively from a base path
 * 
 */
public class FileSearch implements Runnable {

	private String initPath;
	private String fileName;
	
	public FileSearch(String initPath, String fileName) {
		this.initPath = initPath;
		this.fileName = fileName;
	}
	
	private void fileProcess(File file) throws InterruptedException {
		
		if (file.getName().equals(fileName)) {
			System.out.println(Thread.currentThread().getName() + ": " + file.getAbsolutePath());
		}
		
		// Note the static call. It says : Have I been interrupted since the last time I checked?
		// This will RESET the internal interrupted flag of the current thread.
		// Hence for two consecutive calls to the same method, the second one will always return false.
		// This static method is a MISNOMER
		if (Thread.interrupted()) { 
			throw new InterruptedException("fileProcess interruption.");
		}
	}
	
	private void directoryProcess(File file) throws InterruptedException {
		
		File list[] = file.listFiles();
		
		if (list != null) {
			for (File f : list) {
				if (f.isDirectory()) {
					directoryProcess(f);
				}
				else {
					fileProcess(f);
				}
			}
		}
		
		if (Thread.interrupted()) {
			throw new InterruptedException("directoryProcess interruption.");
		}
	}
	
	@Override
	public void run() {
		File file = new File(initPath);
		try {
			if (file.isDirectory()) {
				directoryProcess(file);
			}
			else {
				fileProcess(file);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		
		Thread search = new Thread(new FileSearch("C:\\", "package.json"));
		search.start();
		
		try {
			Thread.sleep(1000);
			search.interrupt();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
