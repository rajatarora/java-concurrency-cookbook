package concurrent.chapter1.t04;

import java.util.Date;

/**
 * @author Rajat Arora
 * 
 * Demostrates the usage of thread.join()
 *
 */
class DataSourcesLoader implements Runnable {

	@Override
	public void run() {
		
		System.out.println("Starting to load data sources: " + new Date());
		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Data sources load ended: " + new Date());
	}
}
