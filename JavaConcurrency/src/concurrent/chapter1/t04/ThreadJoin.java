package concurrent.chapter1.t04;

/**
 * @author Rajat Arora
 * 
 * Demonstrates thread joins
 *
 */
public class ThreadJoin {

	public static void main(String[] args) {
		
		// Create two threads
		Thread dataThread = new Thread(new DataSourcesLoader(), "dataThread");
		Thread networkThread = new Thread(new NetworkConnectionsLoader(), "networkThread");
		
		try {
			
			// Start both. They are running in parallel now.
			dataThread.start();
			networkThread.start();
			
			// Join both with the MAIN thread.
			// Now MAIN thread has to wait until both of them finish
			dataThread.join();
			networkThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// once both these threads finish, MAIN thread prints this and exits.
		System.out.println("Both threads finished.");

		// In case there was no thread joins, MAIN would have started those threads and continued its own execution
		// So it would have finished "Both threads finished" message first, before either of the threads finished.
		
		// join also takes millis as argument. In that case, the MAIN thread will pause, but only upto a maximum time.
	}

}
