package concurrent.chapter1.t04;

import java.util.Date;

public class NetworkConnectionsLoader implements Runnable {

	@Override
	public void run() {
		System.out.println("Starting to load network connections: " + new Date());
		
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Network connections load ended: " + new Date());
	}
}
