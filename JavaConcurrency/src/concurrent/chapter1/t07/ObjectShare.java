package concurrent.chapter1.t07;

class ImplementsRunnable implements Runnable { // IR class
	
	private int counter = 0;
	
	@Override
	public void run() {
		counter++;
		System.out.println(counter);
	}
}

class ExtendsThread extends Thread { // ET class
	
	private int counter = 0;
	
	@Override
	public void run() {
		counter++;
		System.out.println(counter);
	}
}

/**
 * @author Rajat Arora
 * 
 * Here we have two classes : one implements Runnable and one extends Thread
 * In the main method, we start 3 threads each for both classes.
 * We see that we can pass the same object to multiple threads when implementing
 * Runnable, while we have to create a new object for each thread when extending
 * Thread. So the counter in IR class gets shared among multiple threads, but in
 * ET class it resets with each thread.
 *
 * Another difference between IR and ET
 * 
 */
public class ObjectShare {

	public static void main(String[] args) {
		
		ImplementsRunnable ir = new ImplementsRunnable();
		
		for (int i = 0; i < 3; i++) {
			Thread t = new Thread(ir);
			t.start();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for (int i = 0; i < 3; i++) {
			Thread t = new ExtendsThread();
			t.start();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

/* OUTPUT ::
 * 1
 * 2
 * 3 --> ImplementsRunnable
 * 1
 * 1
 * 1 --> ExtendsThread
 */
