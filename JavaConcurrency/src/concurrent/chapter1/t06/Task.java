package concurrent.chapter1.t06;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the Uncaught Exception Handler
 * 
 * Any runnable / thread cannot have a 'throws' clause, so every exception 
 * has to be handled inside the run method itself. However, for RuntimeExceptions
 * this poses a problem, for they cannot be 'caught'. For such things, we
 * can have a class implementing UncaughtExceptionHandler, which defines
 * the default behavior for uncaught exceptions, and add the same to the
 * thread using setUncaughtExceptionHandler method.
 *
 */
public class Task implements Runnable {

	@Override
	public void run() {
		
		// Welcome to the uncaught exception
		//noinspection ResultOfMethodCallIgnored
		Integer.parseInt("Hello");
	}
	
	public static void main(String[] args) {
		
		Task task = new Task();
		Thread thread = new Thread(task);
		
		// This is where we set the exception handler
		thread.setUncaughtExceptionHandler(new ExceptionHandler());
		thread.start();
	}
}
