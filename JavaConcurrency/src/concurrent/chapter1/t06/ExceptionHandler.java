package concurrent.chapter1.t06;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 * @author Rajat Arora
 * 
 * Demostrates the UncaughtExceptionHandler
 *
 */
public class ExceptionHandler implements UncaughtExceptionHandler {

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		
		System.out.println(t.getName() + " has an exception.");
		e.printStackTrace();
		
	}

}
