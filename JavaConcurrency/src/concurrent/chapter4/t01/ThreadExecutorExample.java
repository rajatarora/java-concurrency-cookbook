package concurrent.chapter4.t01;

/**
 * @author Rajat Arora
 *
 * Demonstrates a simple ThreadPoolExecutor. Instead of creating a Thread object manually for each separate thread in
 * the application, Java's executor framework can manage the boilerplate tasks required to build a multi-threaded
 * application.
 *
 * The executor framework makes use of ThreadPools, which are sort of containers for Thread objects. Whenever a Runnable
 * wants to execute, it can request a thread from the pool. The thread is returned to the pool once the Runnable
 * finishes its execution.
 *
 * In this example, we're using a CachedThreadPool, which simply creates a new thread whenever one is required and there
 * isn't any free thread in the pool. This is the simplest type of ThreadPool Java has to offer.
 */
public class ThreadExecutorExample {

    public static void main (String[] args) {

        Server server = new Server();
        for (int i = 0; i < 100; i++) {
            Task task = new Task();
            server.executeTask(task);
        }
        server.endServer();
    }
}
