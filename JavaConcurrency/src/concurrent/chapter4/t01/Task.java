package concurrent.chapter4.t01;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 *
 * Demonstrates as simple ThreadPoolExecutor. For complete description see ThreadExecutorExample.java.
 * This class describes the basic task performed by the ThreadPoolExecutor. It simply prints out the time
 * it was created, the time it was started, and then proceeds to wait for a random number of seconds.
 */
public class Task implements Runnable {

    private Date initDate;

    Task() {
        this.initDate = new Date();
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " created on: " + initDate);
        System.out.println(Thread.currentThread().getName() + " started on: " + new Date());

        try {
            long duration = (long) (Math.random()*10);
            System.out.println(Thread.currentThread().getName() + " working for: " + duration + " seconds.");
            TimeUnit.SECONDS.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " finished on: " + new Date());
    }
}
