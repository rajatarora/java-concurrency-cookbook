package concurrent.chapter4.t01;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author Rajat Arora
 *
 * Demonstrates a simple ThreadPoolExecutor. For complete description see ThreadExecutorExample.java.
 */
class Server {

    private ThreadPoolExecutor executor;

    Server() {
        executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
    }

    void executeTask(Task task) {
        System.out.println("Server: A new task has arrived.");

        executor.execute(task);

        System.out.println("Server: Pool Size: " + executor.getPoolSize());
        System.out.println("Server: Active Count: " + executor.getActiveCount());
        System.out.println("Server: Completed Tasks: " + executor.getCompletedTaskCount());
    }

    void endServer() {
        executor.shutdown();
    }
}
