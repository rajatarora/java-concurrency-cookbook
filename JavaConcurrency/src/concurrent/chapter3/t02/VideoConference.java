package concurrent.chapter3.t02;

import java.util.concurrent.CountDownLatch;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the CountDownLatch mechanism to wait for 
 * n events to occur. For complete description see CDLatch.java
 *
 */
public class VideoConference implements Runnable {

	private CountDownLatch controller;
	
	public VideoConference(int number) {
		controller = new CountDownLatch(number);
	}
	
	public void arrive(String name) {
		System.out.println(name + " has arrived.");
		controller.countDown();
		System.out.println("Waiting for " + controller.getCount() + " participants.");
	}
	
	@Override
	public void run() {
		System.out.println("VideoConference initialized. Total " + controller.getCount() + " participants.");
		try {
			controller.await();
			System.out.println("All participants have arrived.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
