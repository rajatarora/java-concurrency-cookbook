package concurrent.chapter3.t02;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the CountDownLatch mechanism to wait for 
 * n events to occur. 
 * 
 * The CountDownLatch is initialized with a specific count n. 
 * The thread waiting on it calls the latch.await() method, and
 * as events keep happening, the latch counts down. Once the
 * count reaches zero, await() exits and the thread wakes up.
 * 
 * In this example, the VideoConference has x participants. The
 * conference initializes by waiting for them. Once all participants
 * arrive, the conference starts.
 *
 */
class CDLatch {

	public static void main(String[] args) {
		
		int numParticipants = 10;
		
		VideoConference conference = new VideoConference(numParticipants);
		Thread conferenceThread = new Thread(conference);
		conferenceThread.start();
		
		for (int i = 0; i < numParticipants; i++) {
			Participant participant = new Participant(conference, "Participant " + i);
			Thread participantThread = new Thread(participant);
			participantThread.start();
		}
	}
}
