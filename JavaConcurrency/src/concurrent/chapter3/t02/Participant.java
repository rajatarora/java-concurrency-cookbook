package concurrent.chapter3.t02;

import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the CountDownLatch mechanism to wait for 
 * n events to occur. For complete description see CDLatch.java
 *
 */
public class Participant implements Runnable {

	private VideoConference conference;
	private String name;
	
	public Participant(VideoConference conference, String name) {
		this.conference = conference;
		this.name = name;
	}
	
	@Override
	public void run() {
		long duration = (long) (Math.random()*10);
		try {
			TimeUnit.SECONDS.sleep(duration);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		conference.arrive(name);
	}
}
