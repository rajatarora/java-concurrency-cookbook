package concurrent.chapter3.t01;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the usage of Semaphores to control concurrent
 * access to multiple copies of the same resource. 
 * 
 * Here a Semaphore object is initialized with as many permits
 * as the number of printers. Whenever a print job is started,
 * a semaphore is acquired, a printer assignment is done, and
 * the job is executed using that printer. After the job is done,
 * the printer is returned to the pool and the semaphore is 
 * released.
 *
 */
public class PrintQueue {

	private Semaphore semaphore;
	private boolean[] freePrinters;
	private Lock lockPrinters;
	
	public PrintQueue(int numPrinters) {
		this.semaphore = new Semaphore(numPrinters);
		this.freePrinters = new boolean[numPrinters];
		for (int i = 0; i < numPrinters; i++) {
			this.freePrinters[i] = true;
		}
		this.lockPrinters = new ReentrantLock();
	}
	
	public void printJob(Object toPrint) {
		try {
			semaphore.acquire();
			int assignedPrinter = getPrinter();
			long duration = (long) (Math.random()*10);
			System.out.println(Thread.currentThread().getName() + " printing for " + duration 
					+ " on Printer : " + assignedPrinter);
			TimeUnit.SECONDS.sleep(duration);	
			freePrinters[assignedPrinter] = true;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			semaphore.release();
		}
	}
	
	private int getPrinter() {
		int result = -1;
		
		try {
			lockPrinters.lock();
			for (int i = 0; i < freePrinters.length; i++) {
				if (freePrinters[i]) {
					result = i;
					freePrinters[i] = false;
					break;
				}
			}
		} finally {
			lockPrinters.unlock();
		}
		
		return result;
	}
}
