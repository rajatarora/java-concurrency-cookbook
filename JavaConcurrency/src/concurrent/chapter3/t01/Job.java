package concurrent.chapter3.t01;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the usage of Semaphores to control concurrent
 * access to multiple copies of the same resource. 
 *
 */
public class Job implements Runnable {

	private PrintQueue printQueue;
	
	public Job (PrintQueue printQueue) {
		this.printQueue = printQueue;
	}
	
	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName() + " starting to print.");
		printQueue.printJob(new Object());
		System.out.println(Thread.currentThread().getName() + " finished printing.");
	}
}
