package concurrent.chapter3.t01;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the usage of Semaphores to control concurrent
 * access to multiple copies of the same resource. 
 * 
 * Here x threads try to print documents using y printers. If
 * x > y, threads have to wait in line to get the print job done.
 *
 */
public class PrintSemaphore {

	public static void main(String[] args) {
		
		long start = System.currentTimeMillis();
		
		int numPrinters = 3;
		int numThreads = 20;
		
		PrintQueue printQueue = new PrintQueue(numPrinters);
		
		Thread[] printThreads = new Thread[numThreads];
		
		for (int i = 0; i < numThreads; i++) {
			printThreads[i] = new Thread(new Job(printQueue));
		}
		
		for (int i = 0; i < numThreads; i++) {
			printThreads[i].start();
		}
		
		for (int i = 0; i < numThreads; i++) {
			try {
				printThreads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		long end = System.currentTimeMillis();
		
		System.out.println("Total Time : " + (end-start));
	}
}
