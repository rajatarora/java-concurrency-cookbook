package concurrent.chapter3.t03;

import java.util.concurrent.CyclicBarrier;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the CyclicBarrier technique for implementing divide-and-conquer
 * type of tasks. It is a synchronization aid that allows a set of threads to all
 * wait for each other to reach a common point.
 * 
 * A cyclic barrier takes two arguments - no. of threads to wait on, and a Runnable
 * that needs to be run after all threads complete. 
 * 
 * In this example, we have a very big matrix that needs to be searched for no. of 
 * occurrences of a specific number. We divide the work into 5 worker threads. Each
 * worker thread, after completing, calls on await() of the cyclic barrier. Once all
 * threads call on await(), the barrier runs the grouper thread to tally the results.
 *
 */
class CycBarrier {

	public static void main(String[] args) {
		
		final int ROWS = 10000;
		final int LENGTH = 1000;
		final int SEARCH = 5;
		final int PARTICIPANTS = 5;
		final int ROWS_PARTICIPANT = ROWS/PARTICIPANTS;

		long startTime = System.currentTimeMillis();
		
		MatrixMock mock = new MatrixMock(ROWS, LENGTH, SEARCH);
		Results results = new Results(ROWS);
		Grouper grouper = new Grouper(results);

		// Define the barrier with a certain number of PARTICIPANTS,
        // and a grouper task that will run after all threads complete
		CyclicBarrier barrier = new CyclicBarrier(PARTICIPANTS, grouper);
		Searcher[] searchers = new Searcher[PARTICIPANTS];
		Thread[] threads = new Thread[PARTICIPANTS];
		
		for (int i = 0; i < PARTICIPANTS; i++) {
			int start = i*ROWS_PARTICIPANT;
			int end = start + ROWS_PARTICIPANT;
			searchers[i] = new Searcher(start, end, mock, results, SEARCH, barrier);
			threads[i] = new Thread(searchers[i]);
			threads[i].start();
		}

		// Join all threads
        for (int i = 0; i < PARTICIPANTS; i++) {
		    try {
                threads[i].join();
            } catch (InterruptedException e) {
		        e.printStackTrace();
            }
        }

		long endTime = System.currentTimeMillis();
		System.out.println("Total time : " + (endTime-startTime));
	}
}
