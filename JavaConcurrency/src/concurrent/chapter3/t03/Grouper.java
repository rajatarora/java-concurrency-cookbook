package concurrent.chapter3.t03;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the CyclicBarrier technique for implementing divide-and-conquer
 * type of tasks. 
 * 
 * This class is run after all searcher threads complete.
 *
 */
public class Grouper implements Runnable {

	private Results results;
	
	public Grouper(Results results) {
		this.results = results;
	}
	
	@Override
	public void run() {
		int finalResult = 0;
		int[] data = results.getData();
		for (int number : data) {
			finalResult += number;
		}
		System.out.println("Total occurrences : " + finalResult);
	}
}
