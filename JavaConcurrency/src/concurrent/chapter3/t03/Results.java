package concurrent.chapter3.t03;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the CyclicBarrier technique for implementing divide-and-conquer
 * type of tasks. 
 * 
 * This is where results are stored for the computation, row-wise.
 *
 */
public class Results {
	
	private int[] data;
	
	public Results(int size) {
		this.data = new int[size];
	}
	
	public void setData(int position, int value) {
		this.data[position] = value;
	}
	
	public int[] getData() {
		return this.data;
	}
}
