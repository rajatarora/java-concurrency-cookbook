package concurrent.chapter3.t03;

import java.util.Random;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the CyclicBarrier technique for implementing divide-and-conquer
 * type of tasks. 
 * 
 * This class mocks a very big matrix, filling it with random numbers from 0 to 10.
 *
 */
class MatrixMock {

	private int[][] data;
	
	MatrixMock(int size, int length, int number) {
		int counter = 0;
		this.data = new int[size][length];
		
		Random random = new Random();
		
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < length; j++) {
				data[i][j] = random.nextInt(10);
				if (data[i][j] == number) {
					counter++;
				}
			}
		}
		System.out.printf("Total occurrences of %d : %d%n", number, counter);
	}
	
	int[] getRow(int rowNumber) {
		if (rowNumber >= 0 && rowNumber < data.length) {
			return data[rowNumber];
		}
		return null;
	}
}
