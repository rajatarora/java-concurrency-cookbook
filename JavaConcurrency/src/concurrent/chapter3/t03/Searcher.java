package concurrent.chapter3.t03;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author Rajat Arora
 * 
 * Demonstrates the CyclicBarrier technique for implementing divide-and-conquer
 * type of tasks. 
 * 
 * Multiple copies of this class search for the number concurrently.
 *
 */
public class Searcher implements Runnable {

	private int firstRow;
	private int lastRow;
	
	private MatrixMock mock;
	private Results results;
	private int number;
	
	private CyclicBarrier barrier;
	
	Searcher(int firstRow, int lastRow, MatrixMock mock, Results results, int number, CyclicBarrier barrier) {
		this.firstRow = firstRow;
		this.lastRow = lastRow;
		this.mock = mock;
		this.results = results;
		this.number = number;
		this.barrier = barrier;
	}
	
	@Override
	public void run() {
		
		System.out.println("Processing rows " + firstRow + " to " + lastRow);
		for (int i = firstRow; i < lastRow; i++) { 
			int counter = 0;
			int[] row = mock.getRow(i);
            for (int aRow : row) {
                if (aRow == number) {
                    counter++;
                }
            }
			results.setData(i, counter);
		}
		System.out.println("Rows " + firstRow + " to " + lastRow + " processed");
		
		try {
			barrier.await();
		} catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
		}
	}
}
