package concurrent.chapter3.t04;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 * @author Rajat Arora
 *
 * Demonstrates Phaser technique by having multiple threads look for log files in parallel.
 */
public class FileSearch implements Runnable {

    private String initPath;
    private String end;
    private List<String> results;
    private Phaser phaser;

    FileSearch(String initPath, String end, Phaser phaser) {
        this.initPath = initPath;
        this.end = end;
        this.phaser = phaser;
        this.results = new ArrayList<>();
    }

    private void directoryProcess(File file) {
        File[] list = file.listFiles();
        if (list != null) {
            for (File aList : list) {
                if (aList.isDirectory()) {
                    directoryProcess(aList);
                } else {
                    fileProcess(aList);
                }
            }
        }
    }

    private void fileProcess(File file) {
        if (file.getName().endsWith(end)) {
            results.add(file.getAbsolutePath());
        }
    }

    private void filterResults() {
        List<String> newResults = new ArrayList<>();
        long actualDate = new Date().getTime();

        for (String result : results) {
            File file = new File(result);
            long fileDate = file.lastModified();
            if ((actualDate - fileDate) < TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)) {
                newResults.add(result);
            }
        }
        results = newResults;
    }

    private boolean checkResults() {
        if (results.isEmpty()) {
            System.out.println(Thread.currentThread().getName() + " Phase : " + phaser.getPhase() + " : 0 Results.");
            phaser.arriveAndDeregister();
            return false;
        }
        else {
            System.out.println(Thread.currentThread().getName() + " Phase : " + phaser.getPhase() + " : " + results.size() + " Results.");
            phaser.arriveAndAwaitAdvance();
            return true;
        }
    }

    private void showInfo() {
        for (String result : results) {
            File file = new File(result);
            System.out.println(Thread.currentThread().getName() + " : " + file.getAbsolutePath());
        }
        phaser.arriveAndAwaitAdvance();
    }

    @Override
    public void run() {
        phaser.arriveAndAwaitAdvance();
        System.out.println("Starting " + Thread.currentThread().getName());
        File file = new File(initPath);
        if (file.isDirectory()) {
            directoryProcess(file);
        }
        if (!checkResults()) return;

        filterResults();

        if (!checkResults()) return;

        showInfo();
        phaser.arriveAndDeregister();
        System.out.println("Completed : " + Thread.currentThread().getName());
    }
}
