package concurrent.chapter3.t04;

import java.util.concurrent.Phaser;

/**
 * @author Rajat Arora
 *
 * Demonstrates Phaser technique by having multiple threads look for log files in parallel.
 */
public class PhasedSearch {

    public static void main(String[] args) {

        Phaser phaser = new Phaser(3);

        FileSearch system = new FileSearch("C:\\Windows\\", "log", phaser);
        FileSearch apps = new FileSearch("C:\\Program Files\\", "log", phaser);
        FileSearch docs = new FileSearch("C:\\Documents And Settings\\", "log", phaser);

        Thread systemThread = new Thread(system, "System");
        Thread appsThread = new Thread(apps, "Apps");
        Thread docsThread = new Thread(docs, "Docs");

        systemThread.start();
        appsThread.start();
        docsThread.start();

        try {
            systemThread.join();
            appsThread.join();
            docsThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Phaser terminated : " + phaser.isTerminated());
    }
}
